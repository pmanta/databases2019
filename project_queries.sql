-- JOIN queries get permanent employees and contractor employees --
select * from 
cdatabases.employee inner join cdatabases.permanent on id = employee_id;

select * from 
cdatabases.employee inner join cdatabases.contractor on id = employee_id;

-- Aggregate query get the avg paycheck of employees--
select avg(paycheck) from cdatabases.employee;

-- Group by query get number of copies for every book--
select book_isbn, count(*) as num_of_copies from cdatabases.copy
group by book_isbn;

-- Order by query 2 highest payed employees--
select * from cdatabases.employee
order by paycheck DESC
LIMIT 2;

-- Group by having query books with copies more than 1--
select book_isbn from cdatabases.copy
group by book_isbn having count(*) > 1;

-- Nested query available books --
select isbn,title,year,pages,publisher_id, count(copyNumber) as copies from
(select * from book inner join available_books_view on isbn=book_isbn) as s
group by s.isbn;