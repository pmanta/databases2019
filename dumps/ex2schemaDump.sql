CREATE DATABASE  IF NOT EXISTS `ex2schema` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ex2schema`;
-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: ex2schema
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `recipient_uid` int(11) NOT NULL,
  `sender_uid` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `message` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`recipient_uid`,`sender_uid`,`time`),
  KEY `fk_message_2_idx` (`sender_uid`),
  CONSTRAINT `fk_message_1` FOREIGN KEY (`recipient_uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_2` FOREIGN KEY (`sender_uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,2,'2018-12-31 22:00:01','hello there'),(1,6,'2019-01-20 18:15:02','could you be loved ???'),(1,6,'2019-01-20 18:20:02','blah blah blah blah :D'),(2,1,'2018-12-31 22:00:02','hi how are you?'),(2,3,'2019-01-01 08:20:02','i am going on vacation'),(2,3,'2019-01-01 08:25:02','how about you?'),(2,12,'2019-05-02 17:15:02','tsipras tsipras tsipras'),(3,2,'2019-01-01 09:00:02','I am also goin on vaction.'),(3,2,'2019-01-01 09:09:02','on crete :D'),(4,2,'2019-01-01 18:09:02','hi!'),(4,2,'2019-01-01 18:10:02','how are you?'),(4,6,'2019-02-02 18:09:02','where?'),(4,12,'2019-04-03 17:15:42','test test test'),(6,4,'2019-02-01 18:09:02','im here!'),(6,4,'2019-02-02 18:15:02','at goodys!!'),(6,7,'2019-03-02 18:15:02','testing the 5 question :P'),(6,7,'2019-04-03 17:15:22','same same same'),(6,7,'2019-04-03 17:17:22','same same same'),(6,12,'2019-04-03 17:15:05','test test test'),(6,12,'2019-04-03 17:15:13','test test test'),(10,11,'2019-01-10 18:15:22','same same'),(11,10,'2019-02-03 19:15:22','same same'),(12,8,'2019-04-03 17:23:22','same same same');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'name1','21022233555'),(2,'name2','2102255333'),(3,'name3','2102255331'),(4,'name4','2102255313'),(5,'name5','2752065999'),(6,'name6','2752065991'),(7,'name7','2752065111'),(8,'name8','2752065222'),(9,'name9','2112003666'),(10,'name10','5556669997'),(11,'name11','55599886658'),(12,'Tsipras','21000000');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-24 11:58:23
