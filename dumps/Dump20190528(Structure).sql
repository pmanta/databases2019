CREATE DATABASE  IF NOT EXISTS `cdatabases` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cdatabases`;
-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: cdatabases
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `birthDay` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `available_books_view`
--

DROP TABLE IF EXISTS `available_books_view`;
/*!50001 DROP VIEW IF EXISTS `available_books_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `available_books_view` AS SELECT 
 1 AS `copyNumber`,
 1 AS `book_isbn`,
 1 AS `bookShelf`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `isbn` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `year` int(11) NOT NULL,
  `pages` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  PRIMARY KEY (`isbn`,`publisher_id`),
  UNIQUE KEY `ISBN_UNIQUE` (`isbn`),
  KEY `fk_books_publishers1_idx` (`publisher_id`),
  CONSTRAINT `fk_books_publishers1` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `book_category`
--

DROP TABLE IF EXISTS `book_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `parent_book_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`parent_book_category_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_book_category_book_category1_idx` (`parent_book_category_id`),
  CONSTRAINT `fk_book_category_book_category1` FOREIGN KEY (`parent_book_category_id`) REFERENCES `book_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `book_has_author`
--

DROP TABLE IF EXISTS `book_has_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_has_author` (
  `book_isbn` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`book_isbn`,`author_id`),
  KEY `fk_books_has_authors_authors1_idx` (`author_id`),
  KEY `fk_books_has_authors_books1_idx` (`book_isbn`),
  CONSTRAINT `fk_book_has_author_author1` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_book_has_author_book1` FOREIGN KEY (`book_isbn`) REFERENCES `book` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `book_has_book_category`
--

DROP TABLE IF EXISTS `book_has_book_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_has_book_category` (
  `book_isbn` int(11) NOT NULL,
  `book_category_id` int(11) NOT NULL,
  PRIMARY KEY (`book_isbn`,`book_category_id`),
  KEY `fk_book_has_book_category_book_category1_idx` (`book_category_id`),
  KEY `fk_book_has_book_category_book1_idx` (`book_isbn`),
  CONSTRAINT `fk_books_has_book_categories_book_categories1` FOREIGN KEY (`book_category_id`) REFERENCES `book_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_books_has_book_categories_books1` FOREIGN KEY (`book_isbn`) REFERENCES `book` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `book_information_view`
--

DROP TABLE IF EXISTS `book_information_view`;
/*!50001 DROP VIEW IF EXISTS `book_information_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `book_information_view` AS SELECT 
 1 AS `isbn`,
 1 AS `title`,
 1 AS `year`,
 1 AS `pages`,
 1 AS `publisher_name`,
 1 AS `publisher_address`,
 1 AS `authors`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `contractor`
--

DROP TABLE IF EXISTS `contractor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor` (
  `employee_id` int(11) NOT NULL,
  `contract_number` int(11) NOT NULL,
  PRIMARY KEY (`employee_id`),
  CONSTRAINT `fk_contractor_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `copy`
--

DROP TABLE IF EXISTS `copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copy` (
  `copyNumber` int(11) NOT NULL AUTO_INCREMENT,
  `book_isbn` int(11) NOT NULL,
  `bookShelf` int(11) NOT NULL,
  PRIMARY KEY (`copyNumber`,`book_isbn`),
  UNIQUE KEY `BookShelf_UNIQUE` (`bookShelf`),
  KEY `fk_copies_books1_idx` (`book_isbn`),
  CONSTRAINT `fk_copies_books1` FOREIGN KEY (`book_isbn`) REFERENCES `book` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`copy_BEFORE_INSERT` BEFORE INSERT ON `copy` FOR EACH ROW
BEGIN
	DECLARE max integer;
    SET @max = (SELECT MAX(copyNumber) FROM `copy` WHERE book_isbn = new.book_isbn);
	IF @max IS NULL THEN
		SET new.copyNumber = 1;
	ELSE
		SET new.copyNumber = @max + 1;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `paycheck` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `address` varchar(100) NOT NULL,
  `birthDay` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_has_copy`
--

DROP TABLE IF EXISTS `member_has_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_has_copy` (
  `member_id` int(11) NOT NULL,
  `copy_copyNumber` int(11) NOT NULL,
  `copy_book_isbn` int(11) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date DEFAULT NULL,
  `returnedDate` date DEFAULT NULL,
  PRIMARY KEY (`copy_copyNumber`,`copy_book_isbn`,`startDate`),
  KEY `fk_member_has_copy_copy1_idx` (`copy_copyNumber`,`copy_book_isbn`),
  KEY `fk_member_has_copy_member1_idx` (`member_id`),
  CONSTRAINT `fk_member_has_copy_copy1` FOREIGN KEY (`copy_copyNumber`, `copy_book_isbn`) REFERENCES `copy` (`copyNumber`, `book_isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_has_copy_member1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`member_has_copy_BEFORE_INSERT` BEFORE INSERT ON `member_has_copy` FOR EACH ROW
BEGIN
	DECLARE num_of_entries integer;
    DECLARE message varchar(100);
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy` WHERE member_id = new.member_id);
    
    
    IF @num_of_entries >= 5 THEN
		SET @message = 'Cannot add new entry for members_id: only 5 active entries are allowed';
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
    END IF;
	
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy` 
						   WHERE member_id = new.member_id
						   AND endDate < NOW()
						   AND returnedDate IS NULL);
    
    IF @num_of_entries > 0 THEN
		SET @message = CONCAT('Cannot add new entry for members_id: the member has to return ', @num_of_entries, ' books');
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
	END IF;
    
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy`
						   WHERE copy_copyNumber = new.copy_copyNumber
                           AND copy_book_isbn = new.copy_book_isbn
                           AND returnedDate IS NULL);
	
    IF @num_of_entries > 0 THEN
		SET @message = 'Cannot add new entry for book: the book is not available';
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
	END IF;
    
    
	IF new.startDate IS NULL THEN
		SET new.startDate = NOW();
	END IF;
	IF new.endDate IS NULL THEN
		SET new.endDate = DATE_ADD(new.startDate, INTERVAL 30 DAY);
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `member_has_copy_notification`
--

DROP TABLE IF EXISTS `member_has_copy_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_has_copy_notification` (
  `member_id` int(11) NOT NULL,
  `copy_copyNumber` int(11) NOT NULL,
  `copy_book_isbn` int(11) NOT NULL,
  `sentDate` date NOT NULL,
  PRIMARY KEY (`member_id`,`copy_copyNumber`,`copy_book_isbn`,`sentDate`),
  KEY `fk_member_has_copy1_copy1_idx` (`copy_copyNumber`,`copy_book_isbn`),
  KEY `fk_member_has_copy1_member1_idx` (`member_id`),
  CONSTRAINT `fk_member_has_copy1_copy1` FOREIGN KEY (`copy_copyNumber`, `copy_book_isbn`) REFERENCES `copy` (`copyNumber`, `book_isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_has_copy1_member1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`member_has_copy_notification_BEFORE_INSERT` BEFORE INSERT ON `member_has_copy_notification` FOR EACH ROW
BEGIN
	DECLARE num_of_entries integer;
    
    
    
    
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy` 
                           WHERE member_id = new.member_id
                           AND copy_copyNumber = new.copy_copyNumber
                           AND copy_book_isbn = new.copy_book_isbn
                           AND returnedDate IS NULL);
                           
	IF @num_of_entries = 0 THEN
		SET @message = 'The member does not have the book';
        SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
	END IF;
        
    
    IF new.sentDate IS NULL THEN
		SET new.sentDate = NOW();
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `permanent`
--

DROP TABLE IF EXISTS `permanent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permanent` (
  `employee_id` int(11) NOT NULL,
  `hireDate` date NOT NULL,
  PRIMARY KEY (`employee_id`),
  CONSTRAINT `fk_permanents_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`permanent_BEFORE_INSERT` BEFORE INSERT ON `permanent` FOR EACH ROW
BEGIN
	IF new.hireDate IS NULL THEN
		SET new.hireDate = NOW();
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publisher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(100) NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Name_UNIQUE` (`name`),
  UNIQUE KEY `Address_UNIQUE` (`address`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'cdatabases'
--

--
-- Dumping routines for database 'cdatabases'
--

--
-- Final view structure for view `available_books_view`
--

/*!50001 DROP VIEW IF EXISTS `available_books_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `available_books_view` AS select `copy`.`copyNumber` AS `copyNumber`,`copy`.`book_isbn` AS `book_isbn`,`copy`.`bookShelf` AS `bookShelf` from `copy` where (not((`copy`.`copyNumber`,`copy`.`book_isbn`) in (select `copy`.`copyNumber`,`copy`.`book_isbn` from (`copy` join `member_has_copy` on(((`copy`.`copyNumber` = `member_has_copy`.`copy_copyNumber`) and (`copy`.`book_isbn` = `member_has_copy`.`copy_book_isbn`) and isnull(`member_has_copy`.`returnedDate`))))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `book_information_view`
--

/*!50001 DROP VIEW IF EXISTS `book_information_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `book_information_view` AS select `b`.`isbn` AS `isbn`,`b`.`title` AS `title`,`b`.`year` AS `year`,`b`.`pages` AS `pages`,`p`.`name` AS `publisher_name`,`p`.`address` AS `publisher_address`,group_concat(`a`.`name` separator ',') AS `authors` from (((`book` `b` join `publisher` `p` on((`p`.`id` = `b`.`publisher_id`))) join `book_has_author` `bha` on((`bha`.`book_isbn` = `b`.`isbn`))) join `author` `a` on((`bha`.`author_id` = `a`.`id`))) group by `b`.`isbn` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-28 12:40:28
