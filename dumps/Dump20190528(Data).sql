CREATE DATABASE  IF NOT EXISTS `cdatabases` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cdatabases`;
-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: cdatabases
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'author1','1990-03-09'),(2,'author2','1990-03-03'),(3,'from ui','2019-05-14');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'book1',1990,1990,1),(2,'book2',1990,300,2),(3,'book3',2000,201,1),(4,'books1',2000,300,2);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book_category`
--

LOCK TABLES `book_category` WRITE;
/*!40000 ALTER TABLE `book_category` DISABLE KEYS */;
INSERT INTO `book_category` VALUES (5,'aaaa',4),(1,'book_cat1',1),(2,'book_cat2',2),(4,'test categories',1);
/*!40000 ALTER TABLE `book_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book_has_author`
--

LOCK TABLES `book_has_author` WRITE;
/*!40000 ALTER TABLE `book_has_author` DISABLE KEYS */;
INSERT INTO `book_has_author` VALUES (1,1),(2,1),(3,1),(1,2),(3,2);
/*!40000 ALTER TABLE `book_has_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book_has_book_category`
--

LOCK TABLES `book_has_book_category` WRITE;
/*!40000 ALTER TABLE `book_has_book_category` DISABLE KEYS */;
INSERT INTO `book_has_book_category` VALUES (1,1),(2,1),(3,2);
/*!40000 ALTER TABLE `book_has_book_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contractor`
--

LOCK TABLES `contractor` WRITE;
/*!40000 ALTER TABLE `contractor` DISABLE KEYS */;
INSERT INTO `contractor` VALUES (2,1);
/*!40000 ALTER TABLE `contractor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `copy`
--

LOCK TABLES `copy` WRITE;
/*!40000 ALTER TABLE `copy` DISABLE KEYS */;
INSERT INTO `copy` VALUES (1,1,1),(2,1,2),(3,1,3),(1,2,4),(2,2,5),(1,3,6),(4,1,7);
/*!40000 ALTER TABLE `copy` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`copy_BEFORE_INSERT` BEFORE INSERT ON `copy` FOR EACH ROW
BEGIN
	DECLARE max integer;
    SET @max = (SELECT MAX(copyNumber) FROM `copy` WHERE book_isbn = new.book_isbn);
	IF @max IS NULL THEN
		SET new.copyNumber = 1;
	ELSE
		SET new.copyNumber = @max + 1;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'emp1','emp1',1500),(2,'emp2','emp2',1000),(3,'aaa','aaa',11),(4,'asdfasd','asdfasfd',111),(5,'test employee','testing',1002),(7,'employee6','employee6',1500);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,'member1','member1','address_member1','1990-03-03'),(2,'member2','member2','address_member2','1990-03-03'),(5,'member3','member3','member3_address','1990-05-07'),(6,'member4','member4','member4_address','1992-05-14'),(7,'member7','member7','member7_address','1999-05-14'),(8,'member8','member8','member8_address','1990-05-10');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `member_has_copy`
--

LOCK TABLES `member_has_copy` WRITE;
/*!40000 ALTER TABLE `member_has_copy` DISABLE KEYS */;
INSERT INTO `member_has_copy` VALUES (1,1,1,'2019-01-01','2019-06-08',NULL),(2,1,2,'2019-01-01','2019-02-02','2019-02-01'),(8,1,3,'2019-04-01','2019-05-12',NULL),(1,2,1,'2019-01-01','2019-06-08',NULL),(2,2,2,'2019-01-01','2019-02-02','2019-02-01'),(7,3,1,'2019-01-01','2019-02-01',NULL);
/*!40000 ALTER TABLE `member_has_copy` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`member_has_copy_BEFORE_INSERT` BEFORE INSERT ON `member_has_copy` FOR EACH ROW
BEGIN
	DECLARE num_of_entries integer;
    DECLARE message varchar(100);
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy` WHERE member_id = new.member_id);
    
    
    IF @num_of_entries >= 5 THEN
		SET @message = 'Cannot add new entry for members_id: only 5 active entries are allowed';
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
    END IF;
	
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy` 
						   WHERE member_id = new.member_id
						   AND endDate < NOW()
						   AND returnedDate IS NULL);
    
    IF @num_of_entries > 0 THEN
		SET @message = CONCAT('Cannot add new entry for members_id: the member has to return ', @num_of_entries, ' books');
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
	END IF;
    
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy`
						   WHERE copy_copyNumber = new.copy_copyNumber
                           AND copy_book_isbn = new.copy_book_isbn
                           AND returnedDate IS NULL);
	
    IF @num_of_entries > 0 THEN
		SET @message = 'Cannot add new entry for book: the book is not available';
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
	END IF;
    
    
	IF new.startDate IS NULL THEN
		SET new.startDate = NOW();
	END IF;
	IF new.endDate IS NULL THEN
		SET new.endDate = DATE_ADD(new.startDate, INTERVAL 30 DAY);
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping data for table `member_has_copy_notification`
--

LOCK TABLES `member_has_copy_notification` WRITE;
/*!40000 ALTER TABLE `member_has_copy_notification` DISABLE KEYS */;
INSERT INTO `member_has_copy_notification` VALUES (7,3,1,'2019-03-01'),(7,3,1,'2019-04-01'),(7,3,1,'2019-05-01'),(8,1,3,'2019-05-12'),(8,1,3,'2019-05-13'),(8,1,3,'2019-05-28');
/*!40000 ALTER TABLE `member_has_copy_notification` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`member_has_copy_notification_BEFORE_INSERT` BEFORE INSERT ON `member_has_copy_notification` FOR EACH ROW
BEGIN
	DECLARE num_of_entries integer;
    
    
    
    
    
    SET @num_of_entries = (SELECT COUNT(*) FROM `member_has_copy` 
                           WHERE member_id = new.member_id
                           AND copy_copyNumber = new.copy_copyNumber
                           AND copy_book_isbn = new.copy_book_isbn
                           AND returnedDate IS NULL);
                           
	IF @num_of_entries = 0 THEN
		SET @message = 'The member does not have the book';
        SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = @message;
	END IF;
        
    
    IF new.sentDate IS NULL THEN
		SET new.sentDate = NOW();
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping data for table `permanent`
--

LOCK TABLES `permanent` WRITE;
/*!40000 ALTER TABLE `permanent` DISABLE KEYS */;
INSERT INTO `permanent` VALUES (1,'2004-11-01');
/*!40000 ALTER TABLE `permanent` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdatabases`.`permanent_BEFORE_INSERT` BEFORE INSERT ON `permanent` FOR EACH ROW
BEGIN
	IF new.hireDate IS NULL THEN
		SET new.hireDate = NOW();
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping data for table `publisher`
--

LOCK TABLES `publisher` WRITE;
/*!40000 ALTER TABLE `publisher` DISABLE KEYS */;
INSERT INTO `publisher` VALUES (1,'publisher1','address_publisher1',1990),(2,'publisher2','address_publisher2',1990),(3,'test_pub','test pub address',1992),(5,'pub4','pub4',1203);
/*!40000 ALTER TABLE `publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cdatabases'
--

--
-- Dumping routines for database 'cdatabases'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-28 12:40:38
