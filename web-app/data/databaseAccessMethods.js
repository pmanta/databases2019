/* eslint-disable no-console */
const pool = require('./config.js');

async function getMembers() {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM member', (error, result) => {
            if (error) reject(error);
            resolve(result);
        });
    });
}

async function getAuthors() {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM author', (error, result) => {
            if (error) reject(error);

            resolve(result);
        });
    });
}

async function getBooks() {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM book', (error, result) => {
            if (error) reject(error);

            resolve(result);
        });
    });
}

async function getPublishers() {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM publisher', (error, result) => {
            if (error) reject(error);

            resolve(result);
        });
    });
}

async function getBookCategories() {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM book_category', (error, result) => {
            if (error) reject(error);

            resolve(result);
        });
    });
}

/*
 * TODO: make a stored procedure for this kind of request
 * and serve the employees with their ISA relation status
 */
async function getEmployees() {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM employee', (error, result) => {
            if (error) reject(error);

            resolve(result);
        });
    });
}

/* Get specific element */
async function getMemberById(id) {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM member WHERE id = ?', id, (error, result) => {
            if (error) reject(error);

            resolve(result[0]);
        });
    });
}

async function getAuthorById(id) {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM author WHERE id = ?', id, (error, result) => {
            if (error) reject(error);

            resolve(result[0]);
        });
    });
}

async function getBookById(isbn) {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM book WHERE isbn = ?', isbn, (error, result) => {
            if (error) reject(error);

            resolve(result[0]);
        });
    });
}

async function getPublisherById(id) {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM publisher WHERE id = ?', id, (error, result) => {
            if (error) reject(error);

            resolve(result[0]);
        });
    });
}

async function getBookCategoryById(id) {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM book_category WHERE id = ?', id, (error, result) => {
            if (error) reject(error);

            resolve(result[0]);
        });
    });
}

async function getEmployeeById(id) {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM employee WHERE id = ?', id, (error, result) => {
            if (error) reject(error);

            resolve(result[0]);
        });
    });
}

async function postMember(data) {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO member SET name=?, surname=?, address=?, birthDay=?',
            [data.name, data.surname, data.address, data.birthDay],
            (error, result) => {
                if (error) {
                    console.log('Post authors error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    resolve(result.insertId);
                }
            });
    });
}

async function postAuthor(data) {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO author SET name=?, birthDay=?',
            [data.name, data.birthDay],
            (error, result) => {
                if (error) {
                    console.log('Post authors error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    resolve(result.insertId);
                }
            });
    });
}

async function postBook(data) {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO book SET isbn=?, title=?, year=?, pages=?, publisher_id=?',
            [data.isbn, data.title, data.year, data.pages, data.publisher_id],
            (error, result) => {
                if (error) {
                    console.log('Post books error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(data.isbn);
                }
            });
    });
}

async function postPublisher(data) {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO publisher SET name=?, address=?, year=?',
            [data.name, data.address, data.year],
            (error, result) => {
                if (error) {
                    console.log('Post publisher error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(result.insertId);
                }
            });
    });
}

async function postBookCategory(data) {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO book_category SET name=?, parent_book_category_id=?',
            [data.name, data.parent_book_category_id],
            (error, result) => {
                if (error) {
                    console.log('Post book category error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(result.insertId);
                }
            });
    });
}

async function postEmployee(data) {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO employee SET name=?, surname=?, paycheck=?',
            [data.name, data.surname, data.paycheck],
            (error, result) => {
                if (error) {
                    console.log('Post employee error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(result.insertId);
                }
            });
    });
}

async function delMemberById(id) {
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM member WHERE id=?',
            [id],
            (error, result) => {
                if (error) {
                    console.log('Delete members error ----> ', error);
                    reject(error);
                } else {
                    // return the affected Rows if 0 then return 404 not found
                    resolve(result.affectedRows);
                }
            });
    });
}

async function delAuthorById(id) {
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM author WHERE id=?',
            [id],
            (error, result) => {
                if (error) {
                    console.log('Delete author error ----> ', error);
                    reject(error);
                } else {
                    // return the affected Rows if 0 then return 404 not found
                    resolve(result.affectedRows);
                }
            });
    });
}

async function delBookByIsbn(isbn) {
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM book WHERE isbn=?',
            [isbn],
            (error, result) => {
                if (error) {
                    console.log('Delete book error ----> ', error);
                    reject(error);
                } else {
                    // return the affected Rows if 0 then return 404 not found
                    resolve(result.affectedRows);
                }
            });
    });
}

async function delPublisherById(id) {
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM publisher WHERE id=?',
            [id],
            (error, result) => {
                if (error) {
                    console.log('Delete publisher error ----> ', error);
                    reject(error);
                } else {
                    // return the affected Rows if 0 then return 404 not found
                    resolve(result.affectedRows);
                }
            });
    });
}

async function delBookCategoryById(id) {
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM book_category WHERE id=?',
            [id],
            (error, result) => {
                if (error) {
                    console.log('Delete book category error ----> ', error);
                    reject(error);
                } else {
                    // return the affected Rows if 0 then return 404 not found
                    resolve(result.affectedRows);
                }
            });
    });
}

async function delEmployeeById(id) {
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM employee WHERE id=?',
            [id],
            (error, result) => {
                if (error) {
                    console.log('Delete employee error ----> ', error);
                    reject(error);
                } else {
                    // return the affected Rows if 0 then return 404 not found
                    resolve(result.affectedRows);
                }
            });
    });
}

async function updateMemberById(data, id) {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE member SET name=?, surname=?, address=?, birthDay=? WHERE id=?',
            [data.name, data.surname, data.address, data.birthDay, id],
            (error, result) => {
                if (error) {
                    console.log('PUT members error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    resolve(result.affectedRows);
                }
            });
    });
}

async function updateAuthorById(data, id) {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE author SET name=?, birthDay=? WHERE id=?',
            [data.name, data.birthDay, id],
            (error, result) => {
                if (error) {
                    console.log('PUT authors error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    resolve(result.affectedRows);
                }
            });
    });
}

// Changing isbn
async function updateBookByIsbn(data, isbn) {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE book SET isbn=?, title=?, year=?, pages=?, publisher_id=? WHERE isbn=?',
            [data.isbn, data.title, data.year, data.pages, data.publisher_id, isbn],
            (error, result) => {
                if (error) {
                    console.log('PUT books error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(result.affectedRows);
                }
            });
    });
}

async function updatePublisherById(data, id) {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE publisher SET name=?, address=?, year=? WHERE id=?',
            [data.name, data.address, data.year, id],
            (error, result) => {
                if (error) {
                    console.log('Post publisher error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(result.affectedRows);
                }
            });
    });
}

async function updateBookCategoryById(data, id) {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE book_category SET name=?, parent_book_category_id=? WHERE id=?',
            [data.name, data.parent_book_category_id, id],
            (error, result) => {
                if (error) {
                    console.log('Post book category error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(result.affectedRows);
                }
            });
    });
}

async function updateEmployeeById(data, id) {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE employee SET name=?, surname=?, paycheck=? WHERE id=?',
            [data.name, data.surname, data.paycheck, id],
            (error, result) => {
                if (error) {
                    console.log('Post employee error ----> ', error);
                    reject(error);
                } else {
                    // Maybe seach for this id
                    console.log(result);
                    resolve(result.affectedRows);
                }
            });
    });
}

/* TODO : nested query */
const queries = [
    'select * from employee inner join permanent on id = employee_id',
    'select * from employee inner join contractor on id = employee_id',
    'select avg(paycheck) from employee',
    'select book_isbn, count(*) as num_of_copies from copy group by book_isbn',
    'select * from employee order by paycheck DESC LIMIT 2',
    'select book_isbn from copy group by book_isbn having count(*) > 1',
    `select isbn,title,year,pages,publisher_id, count(copyNumber) as copies from
    (select * from book inner join available_books_view on isbn=book_isbn) as s
    group by s.isbn;`,
];

async function runQ(qNum) {
    console.log('Running query "', queries[qNum - 1], '"');

    return new Promise((resolve, reject) => {
        pool.query(queries[qNum - 1], (error, result) => {
            if (error) {
                console.log('error on query ', qNum);
                reject(error);
            } else {
                console.log(result);
                resolve(result);
            }
        });
    });
}


async function getView1() {
    console.log('Getting view 1');

    return new Promise((resolve, reject) => {
        pool.query('select * from book_information_view', (error, result) => {
            if (error) {
                console.log('error on view 1');
                reject(error);
            } else {
                console.log(result);
                resolve(result);
            }
        });
    });
}

async function getView2() {
    console.log('Getting view 1');

    return new Promise((resolve, reject) => {
        pool.query('select * from available_books_view', (error, result) => {
            if (error) {
                console.log('error on view 2');
                reject(error);
            } else {
                console.log(result);
                resolve(result);
            }
        });
    });
}

module.exports = {
    getMembers,
    getAuthors,
    getBooks,
    getPublishers,
    getBookCategories,
    getEmployees,
    getMemberById,
    getAuthorById,
    getBookById,
    getPublisherById,
    getBookCategoryById,
    getEmployeeById,
    postMember,
    postAuthor,
    postBook,
    postPublisher,
    postBookCategory,
    postEmployee,
    delMemberById,
    delAuthorById,
    delBookByIsbn,
    delPublisherById,
    delBookCategoryById,
    delEmployeeById,
    updateMemberById,
    updateAuthorById,
    updateBookByIsbn,
    updatePublisherById,
    updateBookCategoryById,
    updateEmployeeById,
    runQ,
    getView1,
    getView2,
};
