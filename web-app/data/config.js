const mysql = require('mysql');

const config = {
    host: 'localhost',
    user: 'databases19',
    password: 'd4t4b4s3s2019',
    database: 'cdatabases',
    dateStrings: [
        'DATE',
        'DATETIME',
    ]
};

// Create a MySQL pool
const pool = mysql.createPool(config);

module.exports = pool;
