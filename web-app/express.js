/* eslint-disable no-console */
const express = require('express');
const path = require('path');
const app = express();
const bodyParser = require('body-parser');
const dam = require('./data/databaseAccessMethods.js');

const port = process.env.PORT || 3111;


// eslint-disable-next-line func-names
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

/* GET ALL */
app.get('/api/members', (request, response) => {
    console.log('Request for users from ip =====> ', request.ip);
    dam.getMembers()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            response.status(500);
            response.send(e);
        });
});

app.get('/api/authors', (request, response) => {
    console.log('Request for authors from ip =====> ', request.ip);
    dam.getAuthors()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            response.status(500);
            response.send(e);
        });
});

app.get('/api/books', (request, response) => {
    console.log('Request for books from ip =====> ', request.ip);
    dam.getBooks()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            response.status(500);
            response.send(e);
        });
});

app.get('/api/publishers', (request, response) => {
    console.log('Request for publishers from ip =====> ', request.ip);
    dam.getPublishers()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            response.status(500);
            response.send(e);
        });
});

app.get('/api/bookCategories', (request, response) => {
    console.log('Request for book categories from ip =====> ', request.ip);
    dam.getBookCategories()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            response.status(500);
            response.send(e);
        });
});

app.get('/api/employees', (request, response) => {
    console.log('Request for book categories from ip =====> ', request.ip);
    dam.getEmployees()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            response.status(500);
            response.send(e);
        });
});

/* GET SPECIFIC */
app.get('/api/members/:id', (request, response) => {
    console.log('Request for book with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.getMemberById(request.params.id)
        .then((res) => {
            if (!res) response.status(404).send({ message: `Member with id = ${request.params.id} Not found.` });
            else response.send(res);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});


app.get('/api/authors/:id', (request, response) => {
    console.log('Request for book with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.getAuthorById(request.params.id)
        .then((res) => {
            if (!res) response.status(404).send({ message: `Author with id = ${request.params.id} Not found.` });
            else response.send(res);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.get('/api/books/:isbn', (request, response) => {
    console.log('Request for book with isbn ', request.params.isbn, ' from ip =====> ', request.ip);

    dam.getBookById(request.params.isbn)
        .then((res) => {
            if (!res) response.status(404).send({ message: `Book with isbn = ${request.params.isbn} Not found.` });
            else response.send(res);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.get('/api/publishers/:id', (request, response) => {
    console.log('Request for publisher with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.getPublisherById(request.params.id)
        .then((res) => {
            if (!res) response.status(404).send({ message: `Publisher with id = ${request.params.id} Not found.` });
            else response.send(res);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.get('/api/bookCategories/:id', (request, response) => {
    console.log('Request for book categories with id ', request.params.id, ' from ip =====> ', request.ip);
    dam.getBookCategoryById(request.params.id)
        .then((res) => {
            if (!res) response.status(404).send({ message: `Publisher with id = ${request.params.id} Not found.` });
            else response.send(res);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.get('/api/employees/:id', (request, response) => {
    console.log('Request for employee with id ', request.params.id, ' from ip =====> ', request.ip);
    dam.getEmployeeById(request.params.id)
        .then((res) => {
            if (!res) response.status(404).send({ message: `Publisher with id = ${request.params.id} Not found.` });
            else response.send(res);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* POST requests (INSERT) */
app.post('/api/members', (request, response) => {
    console.log('Request for post members from ip =====> ', request.ip);

    dam.postMember(request.body)
        .then((res) => {
            response.send({ id: res, uri: `http://localhost:${port}/api/members/${res}`, type: 'member' });
        })
        .catch((e) => {
            console.log('Error post /api/members ----> ', e);
            response.sendStatus(500);
        });
});

app.post('/api/authors', (request, response) => {
    console.log('Request for post authors from ip =====> ', request.ip);

    dam.postAuthor(request.body)
        .then((res) => {
            response.send({ id: res, uri: `http://localhost:${port}/api/authors/${res}`, type: 'author' });
        })
        .catch((e) => {
            console.log('Error post /api/authors ----> ', e);
            response.sendStatus(500);
        });
});

app.post('/api/books', (request, response) => {
    console.log('Request for post books from ip =====> ', request.ip);

    dam.postBook(request.body)
        .then((res) => {
            response.send({ isbn: res, uri: `http://localhost:${port}/api/books/${res}`, type: 'book' });
        })
        .catch((e) => {
            console.log('Error post /api/books ----> ', e);
            response.sendStatus(500);
        });
});

app.post('/api/publishers', (request, response) => {
    console.log('Request for post publishers from ip =====> ', request.ip);

    dam.postPublisher(request.body)
        .then((res) => {
            response.send({ id: res, uri: `http://localhost:${port}/api/publishers/${res}`, type: 'publisher' });
        })
        .catch((e) => {
            console.log('Error post /api/publishers ----> ', e);
            response.sendStatus(500);
        });
});

app.post('/api/bookCategories', (request, response) => {
    console.log('Request for post bookCategories from ip =====> ', request.ip);

    dam.postBookCategory(request.body)
        .then((res) => {
            response.send({ id: res, uri: `http://localhost:${port}/api/bookCategories/${res}`, type: 'boot category' });
        })
        .catch((e) => {
            console.log('Error post /api/members ----> ', e);
            response.sendStatus(500);
        });
});

app.post('/api/employees', (request, response) => {
    console.log('Request for post employees from ip =====> ', request.ip);

    dam.postEmployee(request.body)
        .then((res) => {
            response.send({ id: res, uri: `http://localhost:${port}/api/employees/${res}`, type: 'employee' });
        })
        .catch((e) => {
            console.log('Error post /api/members ----> ', e);
            response.sendStatus(500);
        });
});

/* DELETE requests */
app.delete('/api/members/:id', (request, response) => {
    console.log('Request for delete member with id ', request.params.id, ' from ip =====> ', request.ip);

    /* find the way we communicate with the database on the delete */
    dam.delMemberById(request.params.id)
        .then((res) => {
            /* 0 means no affected Rows */
            if (res === 0) response.sendStatus(404);
            else response.sendStatus(200);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.delete('/api/authors/:id', (request, response) => {
    console.log('Request for delete author with id ', request.params.id, ' from ip =====> ', request.ip);

    /* find the way we communicate with the database on the delete */
    dam.delAuthorById(request.params.id)
        .then((res) => {
            /* 0 means no affected Rows */
            if (res === 0) response.sendStatus(404);
            else response.sendStatus(200);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.delete('/api/books/:isbn', (request, response) => {
    console.log('Request for delete book with isbn ', request.params.isbn, ' from ip =====> ', request.ip);

    /* find the way we communicate with the database on the delete */
    dam.delBookByIsbn(request.params.isbn)
        .then((res) => {
            /* 0 means no affected Rows */
            if (res === 0) response.sendStatus(404);
            else response.sendStatus(200);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.delete('/api/publishers/:id', (request, response) => {
    console.log('Request for delete publisher with id ', request.params.id, ' from ip =====> ', request.ip);

    /* find the way we communicate with the database on the delete */
    dam.delPublisherById(request.params.id)
        .then((res) => {
            /* 0 means no affected Rows */
            if (res === 0) response.sendStatus(404);
            else response.sendStatus(200);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.delete('/api/bookCategories/:id', (request, response) => {
    console.log('Request for delete book category with id ', request.params.id, ' from ip =====> ', request.ip);

    /* find the way we communicate with the database on the delete */
    dam.delBookCategoryById(request.params.id)
        .then((res) => {
            /* 0 means no affected Rows */
            if (res === 0) response.sendStatus(404);
            else response.sendStatus(200);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.delete('/api/employees/:id', (request, response) => {
    console.log('Request for delete empoloyee with id ', request.params.id, ' from ip =====> ', request.ip);

    /* find the way we communicate with the database on the delete */
    dam.delEmployeeById(request.params.id)
        .then((res) => {
            /* 0 means no affected Rows */
            if (res === 0) response.sendStatus(404);
            else response.sendStatus(200);
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

/* UPDATE requests */
app.put('/api/members/:id', (request, response) => {
    console.log('Request for put method on member with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.updateMemberById(request.body, request.params.id)
        .then((res) => {
            if (res === 0) response.sendStatus(404);
            else response.send({ id: request.params.id, uri: `http://localhost:${port}/api/members/${request.params.id}`, type: 'member' });
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.put('/api/authors/:id', (request, response) => {
    console.log('Request for put method on author with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.updateAuthorById(request.body, request.params.id)
        .then((res) => {
            if (res === 0) response.sendStatus(404);
            else response.send({ id: request.params.id, uri: `http://localhost:${port}/api/authors/${request.params.id}`, type: 'author' });
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.put('/api/books/:isbn', (request, response) => {
    console.log('Request for put method on book with isbn ', request.params.isbn, ' from ip =====> ', request.ip);

    dam.updateBookByIsbn(request.body, request.params.isbn)
        .then((res) => {
            if (res === 0) response.sendStatus(404);
            else response.send({ isbn: request.body.isbn, uri: `http://localhost:${port}/api/books/${request.params.isbn}`, type: 'book' });
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.put('/api/publishers/:id', (request, response) => {
    console.log('Request for put method on publisher with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.updatePublisherById(request.body, request.params.id)
        .then((res) => {
            if (res === 0) response.sendStatus(404);
            else response.send({ id: request.params.id, uri: `http://localhost:${port}/api/publishers/${request.params.id}`, type: 'publisher' });
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.put('/api/bookCategories/:id', (request, response) => {
    console.log('Request for put method on publisher with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.updateBookCategoryById(request.body, request.params.id)
        .then((res) => {
            if (res === 0) response.sendStatus(404);
            else response.send({ id: request.params.id, uri: `http://localhost:${port}/api/bookCategories/${request.params.id}`, type: 'book category' });
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

app.put('/api/employees/:id', (request, response) => {
    console.log('Request for put method on employee with id ', request.params.id, ' from ip =====> ', request.ip);

    dam.updateEmployeeById(request.body, request.params.id)
        .then((res) => {
            if (res === 0) response.sendStatus(404);
            else response.send({ id: request.params.id, uri: `http://localhost:${port}/api/employees/${request.params.id}`, type: 'employee' });
        })
        .catch((e) => {
            response.status(500).send({ error: e });
        });
});

/* Requested queries */
app.get('/api/queries/:number', (request, response) => {
    console.log('Request for getting result from query ', request.params.number, ' from ip ====> ', request.ip);

    dam.runQ(parseInt(request.params.number, 10))
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            console.log(e);
            response.sendStatus(500);
        });
});

app.get('/api/view1', (request, response) => {
    console.log('Request for getting view 1 from ip =====> ', request.ip);

    dam.getView1()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            console.log(e);
            response.sendStatus(500);
        });
});

app.get('/api/view2', (request, response) => {
    console.log('Request for getting view 2 from ip =====> ', request.ip);

    dam.getView2()
        .then((res) => {
            response.send(res);
        })
        .catch((e) => {
            console.log(e);
            response.sendStatus(500);
        });
});

// serve the statis files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.get('*', (req,res) => {
    res.sendFile(path.join(__dirname, 'client/build/index.html'));
});

app.listen(port, () => console.log('App is listening on port ', port));
