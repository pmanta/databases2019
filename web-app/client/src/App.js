import React, { Component } from 'react';
import {BrowserRouter as Router, Switch} from 'react-router-dom';
import Home from './components/Home.jsx';
import BookList from './components/BookList.jsx';
import MemberList from './components/MemberList.jsx';
import EmployeeList from './components/EmployeeList.jsx';
import PublisherList from './components/PublisherList.jsx';
import AuthorList from './components/AuthorList.jsx';
import BookCategoryList from './components/BookCategoryList.jsx';

import PrivateRoute from './components/PrivateRoute.jsx';
import PageDecorator from './components/PageDecorator.jsx';
import BookNew from './components/BookNew.jsx';
import MemberNew from './components/MemberNew.jsx';
import EmployeeNew from './components/EmployeeNew.jsx';
import PublisherNew from './components/PublisherNew.jsx';
import AuthorNew from './components/AuthorNew.jsx';
import BookCategoryNew from './components/BookCategoryNew';

import BookDetails from './components/BookDetails.jsx';
import MemberDetails from './components/MemberDetails.jsx';
import EmployeeDetails from './components/EmployeeDetails.jsx';
import PublisherDetails from './components/PublisherDetails.jsx';
import AuthorDetails from './components/AuthorDetails.jsx';
import BookCategoryDetails from './components/BookCategoryDetails.jsx';
import Joins from './components/Joins.jsx';
import Aggregate from './components/Aggregate.jsx';
import GroupBy from './components/GroupBy.jsx';
import OrderBy from './components/OrderBy.jsx';
import GroupByHaving from './components/GroupByHaving.jsx';
import Nested from './components/Nested.jsx';
import View1 from './components/View1.jsx';
import View2 from './components/View2.jsx';

const NoMatch = () => (
    <div>
        <p>404</p>
    </div>
)

// const emptyDecorator = ({render: Render, ...rest}) => (
//   <div>
//     {Render()}
//   </div>
// )

class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <PrivateRoute path="/" exact decorator={PageDecorator} component={Home} />
                    <PrivateRoute path="/books" exact decorator={PageDecorator} component={BookList} />
                    <PrivateRoute path="/books/new" exact decorator={PageDecorator} component={BookNew} />
                    <PrivateRoute path="/books/:handle" exact decorator={PageDecorator} component={BookDetails} />
                    <PrivateRoute path="/members" exact decorator={PageDecorator} component={MemberList} />
                    <PrivateRoute path="/members/new" exact decorator={PageDecorator} component={MemberNew} />
                    <PrivateRoute path="/members/:handle" exact decorator={PageDecorator} component={MemberDetails} />
                    <PrivateRoute path="/employees" exact decorator={PageDecorator} component={EmployeeList} />
                    <PrivateRoute path="/employees/new" exact decorator={PageDecorator} component={EmployeeNew} />
                    <PrivateRoute path="/employees/:handle" exact decorator={PageDecorator} component={EmployeeDetails} />
                    <PrivateRoute path="/publishers" exact decorator={PageDecorator} component={PublisherList} />
                    <PrivateRoute path="/publishers/new" exact decorator={PageDecorator} component={PublisherNew} />
                    <PrivateRoute path="/publishers/:handle" exact decorator={PageDecorator} component={PublisherDetails} />
                    <PrivateRoute path="/authors" exact decorator={PageDecorator} component={AuthorList} />
                    <PrivateRoute path="/authors/new" exact decorator={PageDecorator} component={AuthorNew} />
                    <PrivateRoute path="/authors/:handle" exact decorator={PageDecorator} component={AuthorDetails} />
                    <PrivateRoute path="/bookCategories" exact decorator={PageDecorator} component={BookCategoryList} />
                    <PrivateRoute path="/bookCategories/new" exact decorator={PageDecorator} component={BookCategoryNew} />}
                    <PrivateRoute path="/bookCategories/:handle" exact decorator={PageDecorator} component={BookCategoryDetails} />

                    {/* The other deliverables */}
                    <PrivateRoute path="/view1" exact decorator={PageDecorator} component={View1} />
                    <PrivateRoute path="/view2" exact decorator={PageDecorator} component={View2} />
                    <PrivateRoute path="/joins" exact decorator={PageDecorator} component={Joins} />
                    <PrivateRoute path="/aggregate" exact decorator={PageDecorator} component={Aggregate} />
                    <PrivateRoute path="/groupBy" exact decorator={PageDecorator} component={GroupBy} />
                    <PrivateRoute path="/orderBy" exact decorator={PageDecorator} component={OrderBy} />
                    <PrivateRoute path="/groupByHaving" exact decorator={PageDecorator} component={GroupByHaving} />
                    <PrivateRoute path="/nested" exact decorator={PageDecorator} component={Nested} />

                    <PrivateRoute decorator={PageDecorator} component={NoMatch} />
                </Switch>
            </Router>
        );
    }
}

export default App;
