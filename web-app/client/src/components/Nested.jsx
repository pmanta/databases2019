import React, { Component } from 'react';
import * as functions from './FunctionLibrary';
import Loading from './Loading';
import CustomHtmlTableRest from './CustomHtmlTableRest';

class Nested extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            loading: true,
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;

        functions.getQuery(7)
            .then((res) => {
                this.update_state({
                    result: res,
                    loading: false,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    render() {
        if (this.state.loading)
            return <Loading />

        return ( <div>
            <section className="content-header">
                <h1>Nested
                    <small>preview of nested query</small>
                </h1>
            </section>
            <div className="callout callout-info pad margin no-print">
                <h4>Get all available books. How many copies and their information</h4>
                select isbn,title,year,pages,publisher_id, count(copyNumber) as copies from
                (select * from book inner join available_books_view on isbn=book_isbn) as s
                group by s.isbn;
            </div>
            <CustomHtmlTableRest  data={this.state.result}/>
        </div> );
    }
}
 
export default Nested;