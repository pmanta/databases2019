import React, { Component } from 'react';
import * as functions from './FunctionLibrary';
import { ToastContainer } from 'react-toastr';
import { Redirect } from 'react-router-dom';

class BookCategoryDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            bookCategories: []    
        }

        this.update_state = functions.update_state.bind(this);
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
    }
    
    componentDidMount() {
        this.mounted = true;
        var { handle } = this.props.match.params
        // Get the product details and show them in the register form
        functions.http_get_json("/bookCategories/" + handle)
            .then(json => {
                // add isloaded true to json :D
                json["isLoaded"] = true;
                this.update_state(json);
            })

        // get the publishers and make loaded true
        functions.http_get_json("/bookCategories")
            .then(json => {
                this.update_state({
                    bookCategories: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    
    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    updateBookCategory(e, toastContainer) {
        console.log("update book category kappa")
        var { handle } = this.props.match.params;

        if (
            !!!this.state.name ||
            !!!this.state.parent_book_category_id
        ) {
            toastContainer.error("Fill all required fields", "Error", {
                closeButton: true
            })
            return;
        }

        var obj = {
            name: this.state.name,
            parent_book_category_id: parseInt(this.state.parent_book_category_id),
        };

        console.log(obj);
        functions.http_put("/bookCategories/" + handle, JSON.stringify(obj))
            .then(json => {
                toastContainer.success("updated book category", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/bookCategories' />
        }
    }

    deleteBookCategory(e, toastContainer) {
        var { handle } = this.props.match.params
        if (window.confirm("Are you sure you want to delete the book category ?"))
            console.log("deleting book category with id :", handle)

        functions.http_delete("/bookCategories/" + handle)
            .then(json => {
                toastContainer.success("deleted book category", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    render() {
        let container;
        return (
            <div>
                {this.renderRedirect()}
                <ToastContainer
                    ref={ref => container = ref}
                    className="toast-top-right"
                />
                
                <section className="content-header">
                    <h1>
                        Book category
                        <small>update book category</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label>Parent book category</label>
                                <select
                                    className="form-control"
                                    id="parent_book_category_id"
                                    onChange={this.propertyChanged}
                                    value={this.validateContext(this.state.parent_book_category_id)}
                                >
                                    <option value='' disabled> Choose... </option>
                                    {/* Here we have to got the publishers and list them by name and take the value of the id and store it to publisher_id */}
                                    {this.state.bookCategories.map((item, index) => 
                                        <option value={item.id} key={index}>{item.name}</option>
                                    )}
                                </select>
                            </div>
                        </div>
                        <div className="box-footer">
                            <button 
                                type="submit" 
                                className="btn btn-primary"
                                onClick={(e) => this.updateBookCategory.bind(this)(e, container)}
                            >
                                Update
                            </button>
                            &nbsp;
                            <button
                                type="submit"
                                className="btn btn-danger"
                                onClick={() => this.update_state({ redirect:true })}
                            >
                                Cancel
                            </button>
                            <button
                                type="submit"
                                className="btn btn-warning pull-right"
                                onClick={(e) => this.deleteBookCategory(e, container)}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                    
                </section>
            </div>
        );
    }
}
 
export default BookCategoryDetails;