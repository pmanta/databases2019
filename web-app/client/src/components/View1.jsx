import React, { Component } from 'react';
import CustomHtmlTableRest from './CustomHtmlTableRest';
import * as functions from './FunctionLibrary';
import Loading from './Loading';

class View1 extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            loading: true,
        }
        this.update_state = functions.update_state.bind(this);
    }
    componentDidMount() {
        this.mounted = true;

        functions.getView1()
            .then((res) => {
                this.update_state({
                    result: res,
                    loading: false,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    render() {
        if (this.state.loading)
            return <Loading />

        return ( <div>
            <section className="content-header">
                <h1>View 1
                    <small>preview of view 1</small>
                </h1>
            </section>
            <div className="callout callout-info pad margin no-print">
                <h4>Get all information for the books</h4>
            </div>
            <CustomHtmlTableRest  data={this.state.result}/>
        </div> );
    }
}
 
export default View1;