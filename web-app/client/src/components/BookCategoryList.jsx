import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTableBookCategories from './CustomHtmlTableBookCategories.jsx';

class BookCategoryList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            members: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the product List from the api
        functions.http_get_json("/bookCategories")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    bookCategories: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const {isLoaded, bookCategories} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Book Categories
                            <small>preview of book categories</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTableBookCategories bookCategories={bookCategories}/>
                    </section>
                </div>
            );
    }
}
 
export default BookCategoryList;