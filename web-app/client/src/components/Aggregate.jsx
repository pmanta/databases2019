import React, { Component } from 'react';
import * as functions from './FunctionLibrary';
import Loading from './Loading';
import CustomHtmlTableRest from './CustomHtmlTableRest';

class Aggregate extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            loading: true,
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;

        functions.getQuery(3)
            .then((res) => {
                this.update_state({
                    result: res,
                    loading: false,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    render() {
        if (this.state.loading)
            return <Loading />

        return ( <div>
            <section className="content-header">
                <h1>Aggregate
                    <small>preview of aggregate query</small>
                </h1>
            </section>
            <div className="callout callout-info pad margin no-print">
                <h4>
                    Get average employee paycheck
                </h4>
                select avg(paycheck) from cdatabases.employee;
            </div>
            <CustomHtmlTableRest  data={this.state.result}/>
        </div> );
    }
}
 
export default Aggregate;