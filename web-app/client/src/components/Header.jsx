import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import * as functions from './FunctionLibrary'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: 'username'
        }

        this.update_state = functions.update_state.bind(this)
    }
    
    componentDidMount() {
        this.mounted = true;

        // Extract username from the jwt token
        this.update_state({
            username: localStorage.getItem("username")
        })
    }

    render() {
        return (
            <header className="main-header">
                <Link to="/" className="logo">
                    <span className="logo-mini"><b>A</b>LT</span>
                    <span className="logo-lg"><b>Admin</b>LTE</span>
                </Link>

                <nav className="navbar navbar-static-top">
                    <a href="# " className="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span className="sr-only">Toggle navigation</span>
                    </a>

                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            <li className="dropdown user user-menu">
                                <a href="# " className="dropdown-toggle" data-toggle="dropdown">
                                    <img src="/dist/img/user2-160x160.jpg" className="user-image" alt="User ="/>
                                    <span className="hidden-xs">{this.state.username}</span>
                                </a>
                                <ul className="dropdown-menu">
                                    <li className="user-header">
                                        <img src="/dist/img/user2-160x160.jpg" className="img-circle" alt="User ="/>
                                        <p>
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <li className="user-footer">
                                        <div className="pull-left">
                                            <a href="# " className="btn btn-default btn-flat">Lock Screen</a>
                                        </div>
                                        <div className="pull-right">
                                            <a href="# " className="btn btn-default btn-flat" onClick={this.userSignOut}>Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}
 
export default Header;