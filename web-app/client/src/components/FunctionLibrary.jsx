
const apiPath = "http://localhost:3111/api"

function handleErrors(res) {
    if (!res.ok)
        throw res.status
    
    return res
}

/* Function that updates the state if this.mounted is set
 * needs to be bound to the component that it gets called
*/
export function update_state(state)
{
    if (this.mounted)
        this.setState(state);
}

export function make2digit(number) {
    return ('0' + number).slice(-2);
}

/* http_get request for the ui with the headers that we
 * need to set and the error handling
 */
export async function http_get_json(endpoint)
{
    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    return fetch(apiPath + "/" + endpoint, {})
        .then(res => res.json())
}

export async function http_post(endpoint, data) {
    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    console.log("http_post")
    return fetch(apiPath + "/" + endpoint, {
        'method': 'POST',
        'headers': {
            'Content-Type': 'application/json',
        },
        'body': data //The data should be JSON.stringified before comming here
    })
        .then(handleErrors)
        .then(res => Promise.resolve(res.json()))
        .catch(status => {
            console.log("Something happened : ", status)
            throw status;
        })
}

export async function http_put(endpoint, data) {
    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    console.log("http_put")
    return fetch(apiPath + "/" + endpoint, {
        'method': 'PUT',
        'headers': {
            'Content-Type': 'application/json',
        },
        'body': data //The data should be JSON.stringified before comming here
    })
        .then(handleErrors)
        .then(res => Promise.resolve(res.json()))
        .catch(status => {
            console.log("Something happened : ", status)
            throw status;
        })
}

/* Dynamically load a javascript "used by components" */
export function loadScript(url, callback) {
    let script = document.createElement('script')
    script.type = 'text/javascript'

    if (script.readyState) {
        //IE
        script.onreadystatechange = function() {
            if (
                script.readyState === 'loaded' ||
                script.readyState === 'complete'
            ) {
                script.onreadystatechange = null
                callback()
            }
        }
    } else {
        //Others
        script.onload = function() {
            callback()
        }
    }

    script.src = url
    document.getElementsByTagName('body')[0].appendChild(script)
}

/*
 * Should be bounded to the component before used
 * Changes the state of the component
 * used from forms when a form property is changed
 */
export function propertyChanged(event) {
    var tmp = {}

    /* update the variable with name as the id of the form */
    tmp[event.target.id] = event.target.value

    /* Update the state of the component */
    update_state.bind(this)(tmp)
}

export function validateContext(context) {
    return !!context ? context : ""
}

export async function http_delete(endpoint) {

    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    return fetch(apiPath + "/" + endpoint, {
        // 'mode': 'cors', //maybe not needed
        'method': 'DELETE',
        'headers': {}
    })
        .then(handleErrors)
        .then(res => Promise.resolve(res.text()))
        .catch(status => {
            console.log("Something happened : ", status)
            throw status;
        })
}


export async function getView1() {
    return http_get_json('view1');
}

export async function getView2() {
    return http_get_json('view2');
}

export async function getQuery(number) {
    return http_get_json(`queries/${number}`);
}