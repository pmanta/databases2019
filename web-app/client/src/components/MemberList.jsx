import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTableMembers from './CustomHtmlTableMembers.jsx';
import CustomHtmlTableRest from './CustomHtmlTableRest.jsx';

class MemberList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            members: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the product List from the api
        functions.http_get_json("/members")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    members: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const {isLoaded, members} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Members
                            <small>preview of members</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTableMembers members={members}/>
                        <CustomHtmlTableRest data={members}/>
                    </section>
                </div>
            );
    }
}
 
export default MemberList;