import React, { Component } from "react";
import * as functions from "./FunctionLibrary";
import { ToastContainer } from "react-toastr";
import Loading from './Loading.jsx';

class BookCategoryNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
        };
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;

        // get the publishers and make loaded true
        functions.http_get_json("/bookCategories")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    bookCategories: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    registerBookCategory(e, toastContainer) {
        if (
            !!!this.state.name ||
            !!!this.state.parent_book_category_id
        ) {
            toastContainer.error("Input form data are invalid", "Error", {
                closeButton: true
            });
            return;
        }

        var obj = {
            name: this.state.name,
            parent_book_category_id: parseInt(this.state.parent_book_category_id),
        };

        functions
            .http_post("/bookCategories", JSON.stringify(obj))
            .then(json => {
                console.log("got ", json);
                toastContainer.success("Registered book.", "Success!", {
                    closeButton: true
                });

                // Empty all fields in the form for adding a new shop
                this.update_state({
                    name: '',
                    parent_book_category_id: '',
                });
            })
            .catch(status => {
                console.log("what is this?");
                toastContainer.error("Status " + status, "Error!", {
                    closeButton: true
                });
            });
    }

    render() {
        let container;

        if (!this.state.isLoaded)
            return <Loading />
        else
            return (
                <div>
                    <ToastContainer
                        ref={ref => (container = ref)}
                        className="toast-top-right"
                    />

                    <section className="content-header">
                        <h1>
                            Book Category
                            <small>add new book category</small>
                        </h1>
                    </section>
                    <section className="content">
                        <div className="box box-primary">
                            {/* <div className="box-header with-border">
                                <h3 className="box-title">Add</h3>
                            </div> */}
                            <div className="box-body">
                                <div className="form-group">
                                    <label htmlFor="name">Name*</label>
                                    <input
                                        type="text"
                                        required
                                        className="form-control"
                                        id="name"
                                        placeholder="Enter name"
                                        value={this.validateContext(this.state.name)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Parent book category</label>
                                    <select
                                        className="form-control"
                                        id="parent_book_category_id"
                                        onChange={this.propertyChanged}
                                        value={this.validateContext(this.state.parent_book_category_id)}
                                    >
                                        <option value='' disabled> Choose... </option>
                                        {/* Here we have to got the publishers and list them by name and take the value of the id and store it to publisher_id */}
                                        {this.state.bookCategories.map((item, index) => 
                                            <option value={item.id} key={index}>{item.name}</option>
                                        )}
                                    </select>
                                </div>
                            </div>
                            <div className="box-footer">
                                <button
                                    type="submit"
                                    className="btn btn-primary"
                                    onClick={e =>
                                        this.registerBookCategory.bind(this)(e, container)
                                    }
                                >
                                    Submit
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            );
    }
}

export default BookCategoryNew;
