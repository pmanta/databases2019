import React, { Component } from 'react';
import * as functions from './FunctionLibrary';
import Loading from './Loading';
import CustomHtmlTableRest from './CustomHtmlTableRest';

class Joins extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            loading: true,
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;

        functions.getQuery(1)
            .then((res1) => {
                functions.getQuery(2)
                    .then((res2) => {
                        this.update_state({
                            result1: res1,
                            result2: res2,
                            loading: false,
                        })
                    })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    render() {
        if (this.state.loading)
            return <Loading />

        return ( <div>
            <section className="content-header">
                <h1>Joins 
                    <small>preview of join queries</small>
                </h1>
            </section>
            <div className="callout callout-info pad margin no-print">
                <h4>
                    Get permanent employees
                </h4>
                select * from 
                cdatabases.employee inner join cdatabases.permanent on id = employee_id;
            </div>
            <CustomHtmlTableRest  data={this.state.result1}/>
            <div className="callout callout-info pad margin no-print">
                <h4>
                    Get contractor employees
                </h4>
                select * from 
                cdatabases.employee inner join cdatabases.contractor on id = employee_id;
            </div>
            <CustomHtmlTableRest  data={this.state.result2}/>
        </div> );
    }
}
 
export default Joins;