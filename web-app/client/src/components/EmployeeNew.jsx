import React, { Component } from "react";
import * as functions from "./FunctionLibrary";
import { ToastContainer } from "react-toastr";

class MemberNew extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    registerEmployee(e, toastContainer) {
        if (
            !!!this.state.name ||
            !!!this.state.surname ||
            !!isNaN(this.state.paycheck)
        ) {
            toastContainer.error("Input form data are invalid", "Error", {
                closeButton: true
            });
            return;
        }

        var obj = {
            name: this.state.name,
            surname: this.state.surname,
            paycheck: parseInt(this.state.paycheck),
        };

        console.log(obj);
        functions
            .http_post("/employees", JSON.stringify(obj))
            .then(json => {
                console.log("got ", json);
                toastContainer.success("Registered employee.", "Success!", {
                    closeButton: true
                });

                // Empty all fields in the form for adding a new shop
                this.update_state({
                    name: '',
                    surname: '',
                    paycheck: ''
                });
            })
            .catch(status => {
                console.log("what is this?");
                toastContainer.error("Status " + status, "Error!", {
                    closeButton: true
                });
            });
    }

    render() {
        let container;
        return (
            <div>
                <ToastContainer
                    ref={ref => (container = ref)}
                    className="toast-top-right"
                />

                <section className="content-header">
                    <h1>
                        Employee
                        <small>add new employee</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="surname">Surname*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="surname"
                                    placeholder="Enter surname"
                                    value={this.validateContext(this.state.surname)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="paycheck">Paycheck*</label>
                                <input
                                    type="number"
                                    required
                                    className="form-control"
                                    id="paycheck"
                                    placeholder="Enter paycheck"
                                    value={this.validateContext(this.state.paycheck)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                        </div>
                        <div className="box-footer">
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={e =>
                                    this.registerEmployee.bind(this)(e, container)
                                }
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default MemberNew;
