import React, { Component } from "react";
import * as functions from "./FunctionLibrary";
import { ToastContainer } from "react-toastr";

class PublisherNew extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    registerPublisher(e, toastContainer) {
        if (
            !!!this.state.name ||
            !!!this.state.address ||
            !!isNaN(this.state.year)
        ) {
            toastContainer.error("Input form data are invalid", "Error", {
                closeButton: true
            });
            return;
        }

        var obj = {
            name: this.state.name,
            address: this.state.address,
            year: parseInt(this.state.year),
        };

        console.log(obj);
        functions
            .http_post("/publishers", JSON.stringify(obj))
            .then(json => {
                console.log("got ", json);
                toastContainer.success("Registered publisher.", "Success!", {
                    closeButton: true
                });

                // Empty all fields in the form for adding a new shop
                this.update_state({
                    name: '',
                    address: '',
                    year: ''
                });
            })
            .catch(status => {
                console.log("what is this?");
                toastContainer.error("Status " + status, "Error!", {
                    closeButton: true
                });
            });
    }

    render() {
        let container;
        return (
            <div>
                <ToastContainer
                    ref={ref => (container = ref)}
                    className="toast-top-right"
                />

                <section className="content-header">
                    <h1>
                        Publisher
                        <small>add new publisher</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="address">Address*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="address"
                                    placeholder="Enter address"
                                    value={this.validateContext(this.state.address)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="year">Year*</label>
                                <input
                                    type="number"
                                    required
                                    className="form-control"
                                    id="year"
                                    placeholder="Enter year"
                                    value={this.validateContext(this.state.year)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                        </div>
                        <div className="box-footer">
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={e =>
                                    this.registerPublisher.bind(this)(e, container)
                                }
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default PublisherNew;
