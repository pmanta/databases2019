import React, { Component } from 'react'
import '../css/custom.css'
import {Link} from 'react-router-dom'

class CustomHtmlTableAuthors extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const { authors } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Birthday</th>
                            </tr>

                            {authors.map((item, index) => (
                                <tr key={index}>
                                    <td>
                                        <Link to={`/authors/${item.id}`}>
                                            {item.id}
                                        </Link>
                                    </td>
                                    <td>{item.name}</td>
                                    <td>{item.birthDay}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTableAuthors