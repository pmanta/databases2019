import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTablePublishers from './CustomHtmlTablePublishers.jsx';

class PublisherList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            publishers: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the product List from the api
        functions.http_get_json("/publishers")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    publishers: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const {isLoaded, publishers} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Publishers
                            <small>preview of publishers</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTablePublishers publishers={publishers}/>
                    </section>
                </div>
            );
    }
}
 
export default PublisherList;