import React, { Component } from 'react';
import '../css/custom.css';
import {Link} from 'react-router-dom';

class CustomHtmlTableBookCategories extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { bookCategories } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Parent Book Category ID</th>
                            </tr>

                            {bookCategories.map((item, index) => (
                                <tr key={index}>
                                    <td>
                                        <Link to={`/bookCategories/${item.id}`}>
                                            {item.id}
                                        </Link>
                                    </td>
                                    <td>{item.name}</td>
                                    <td>
                                        <Link to={`/bookCategories/${item.parent_book_category_id}`}>
                                            {item.parent_book_category_id}
                                        </Link>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTableBookCategories;