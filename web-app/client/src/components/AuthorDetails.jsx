import React, { Component } from 'react';
import Loading from './Loading';
import * as functions from './FunctionLibrary';
import { ToastContainer } from 'react-toastr';
import { Redirect } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

class AuthorDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,        
        }

        this.update_state = functions.update_state.bind(this);
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
    }
    
    componentDidMount() {
        this.mounted = true;
        var { handle } = this.props.match.params
        // Get the product details and show them in the register form
        functions.http_get_json("/authors/" + handle)
            .then(json => {
                // add isloaded true to json :D
                json["isLoaded"] = true;

                // convert the String date to date
                json.birthDay = new Date(json.birthDay)
                
                this.update_state(json);
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    
    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    updateAuthor(e, toastContainer) {
        console.log("update author kappa");
        const make2digit = functions.make2digit;

        var { handle } = this.props.match.params;

        if (
            !!!this.state.name ||
            !!!this.state.birthDay
        ) {
            toastContainer.error("Fill all required fields", "Error", {
                closeButton: true
            })
            return;
        }

        var year = this.state.birthDay.getFullYear();
        var month = this.state.birthDay.getMonth() + 1;
        var day = this.state.birthDay.getDate();

        var date = year + '-' + make2digit(month) + '-' + make2digit(day);

        var obj = {
            name: this.state.name,
            birthDay: date,
        };

        console.log(obj);
        functions.http_put("/authors/" + handle, JSON.stringify(obj))
            .then(json => {
                toastContainer.success("updated author", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/authors' />
        }
    }

    deleteAuthor(e, toastContainer) {
        var { handle } = this.props.match.params
        if (window.confirm("Are you sure you want to delete the author ?"))
            console.log("deleting author with id :", handle)

        functions.http_delete("/authors/" + handle)
            .then(json => {
                toastContainer.success("deleted author", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    render() {
        let container;
        if (!this.state.isLoaded) return (<Loading/>)
        return (
            <div>
                {this.renderRedirect()}
                <ToastContainer
                    ref={ref => container = ref}
                    className="toast-top-right"
                />
                
                <section className="content-header">
                    <h1>
                        Author
                        <small>update author</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="birthDay">Birthday*</label>
                                <DatePicker
                                    className="form-control"
                                    selected={this.state.birthDay}
                                    onChange={(date) => this.update_state.bind(this)({ birthDay: date })}
                                    dateFormat="dd/MM/yyyy"
                                />
                            </div>
                        </div>
                        <div className="box-footer">
                            <button 
                                type="submit" 
                                className="btn btn-primary"
                                onClick={(e) => this.updateAuthor.bind(this)(e, container)}
                            >
                                Update
                            </button>
                            &nbsp;
                            <button
                                type="submit"
                                className="btn btn-danger"
                                onClick={() => this.update_state({ redirect:true })}
                            >
                                Cancel
                            </button>
                            <button
                                type="submit"
                                className="btn btn-warning pull-right"
                                onClick={(e) => this.deleteAuthor(e, container)}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                    
                </section>
            </div>
        );
    }
}
 
export default AuthorDetails;