import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import * as functions from './FunctionLibrary'

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            username: 'username'
        }

        this.update_state = functions.update_state.bind(this)
    }

    componentDidMount() {
        this.mounted = true;
        // Extract the username from the jwt token
        this.update_state({
            username: localStorage.getItem("username")
        })
    }

    render() { 
        return (
            <section className="sidebar">
                <div className="user-panel">
                    <div className="pull-left image">
                        <img src="/dist/img/user2-160x160.jpg" className="img-circle" alt="User"/>
                    </div>
                    <div className="pull-left info">
                        <p>{this.state.username}</p>
                        <a href="# "><i className="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <form action="# " method="get" className="sidebar-form">
                    <div className="input-group">
                        <input type="text" name="q" className="form-control" placeholder="Search..."/>
                        <span className="input-group-btn">
                            <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
                <ul className="sidebar-menu" data-widget="tree">
                    <li className="header">MAIN NAVIGATION</li>
                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-book"></i> <span>Books</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/books"><i className="fa fa-fw fa-table"></i>Book List</Link></li>
                            <li><Link to="/books/new"><i className="fa fa-fw fa-plus-square"></i>Add new</Link></li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-users"></i> <span>Members</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/members"><i className="fa fa-fw fa-table"></i>Member List</Link></li>
                            <li><Link to="/members/new"><i className="fa fa-fw fa-plus-square"></i>Add new</Link></li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-child"></i> <span>Employees</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/employees"><i className="fa fa-fw fa-table"></i>Employee List</Link></li>
                            <li><Link to="/employees/new"><i className="fa fa-fw fa-plus-square"></i>Add new</Link></li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-print"></i> <span>Publishers</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/publishers"><i className="fa fa-fw fa-table"></i>Publisher List</Link></li>
                            <li><Link to="/publishers/new"><i className="fa fa-fw fa-plus-square"></i>Add new </Link></li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-pencil"></i> <span>Authors</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/authors"><i className="fa fa-fw fa-table"></i>Author List</Link></li>
                            <li><Link to="/authors/new"><i className="fa fa-fw fa-plus-square"></i>Add new</Link></li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-tags"></i> <span>Book Categories</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/bookCategories"><i className="fa fa-fw fa-table"></i>Book Category List</Link></li>
                            <li><Link to="/bookCategories/new"><i className="fa fa-fw fa-plus-square"></i>Add new</Link></li>
                        </ul>
                    </li>

                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-clone"></i> <span>Views</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/view1"><i className="fa fa-fw fa-table"></i>View 1</Link></li>
                            <li><Link to="/view2"><i className="fa fa-fw fa-table"></i>View 2</Link></li>
                        </ul>
                    </li>

                    <li className="treeview">
                        <a href="# ">
                            <i className="fa fa-mortar-board"></i> <span>Queries</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><Link to="/joins"><i className="fa fa-fw fa-table"></i>Joins</Link></li>
                            <li><Link to="/aggregate"><i className="fa fa-fw fa-table"></i>Aggregate</Link></li>
                            <li><Link to="/groupBy"><i className="fa fa-fw fa-table"></i>Group By</Link></li>
                            <li><Link to="/orderBy"><i className="fa fa-fw fa-table"></i>Order By</Link></li>
                            <li><Link to="/groupByHaving"><i className="fa fa-fw fa-table"></i>Group By Having</Link></li>
                            <li><Link to="/nested"><i className="fa fa-fw fa-table"></i>Nested</Link></li>
                        </ul>
                    </li>
                </ul>
            </section>
        );
    }
}
 
export default Sidebar;