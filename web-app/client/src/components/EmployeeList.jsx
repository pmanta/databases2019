import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTableEmployees from './CustomHtmlTableEmployees.jsx';

class EmployeeList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            employees: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the product List from the api
        functions.http_get_json("/employees")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    employees: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const {isLoaded, employees} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Employees
                            <small>preview of employees</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTableEmployees employees={employees}/>
                    </section>
                </div>
            );
    }
}
 
export default EmployeeList;