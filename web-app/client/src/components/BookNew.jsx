import React, { Component } from "react";
import * as functions from "./FunctionLibrary";
import { ToastContainer } from "react-toastr";
import Loading from './Loading.jsx';

class BookNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
        };
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;

        // get the publishers and make loaded true
        functions.http_get_json("/publishers")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    publishers: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    registerBook(e, toastContainer) {
        if (
            !!isNaN(this.state.isbn) ||
            !!!this.state.title ||
            !!isNaN(this.state.year) ||
            !!isNaN(this.state.pages) ||
            !!isNaN(this.state.publisher_id)
        ) {
            toastContainer.error("Input form data are invalid", "Error", {
                closeButton: true
            });
            return;
        }

        var obj = {
            isbn: parseInt(this.state.isbn),
            title: this.state.title,
            year: parseInt(this.state.year),
            pages: parseInt(this.state.pages),
            publisher_id: parseInt(this.state.publisher_id),
        };

        console.log(obj);
        functions
            .http_post("/books", JSON.stringify(obj))
            .then(json => {
                console.log("got ", json);
                toastContainer.success("Registered book.", "Success!", {
                    closeButton: true
                });

                // Empty all fields in the form for adding a new shop
                this.update_state({
                    isbn: '',
                    title: '',
                    year: '',
                    pages: '',
                    publisher_id: ''
                });
            })
            .catch(status => {
                console.log("what is this?");
                toastContainer.error("Status " + status, "Error!", {
                    closeButton: true
                });
            });
    }

    render() {
        let container;

        if (!this.state.isLoaded)
            return <Loading />
        else
            return (
                <div>
                    <ToastContainer
                        ref={ref => (container = ref)}
                        className="toast-top-right"
                    />

                    <section className="content-header">
                        <h1>
                            Book
                            <small>add new book</small>
                        </h1>
                    </section>
                    <section className="content">
                        <div className="box box-primary">
                            {/* <div className="box-header with-border">
                                <h3 className="box-title">Add</h3>
                            </div> */}
                            <div className="box-body">
                                <div className="form-group">
                                    <label htmlFor="name">ISBN*</label>
                                    <input
                                        type="text"
                                        required
                                        className="form-control"
                                        id="isbn"
                                        placeholder="Enter isbn"
                                        value={this.validateContext(this.state.isbn)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="address">Title*</label>
                                    <input
                                        type="text"
                                        required
                                        className="form-control"
                                        id="title"
                                        placeholder="Enter Title"
                                        value={this.validateContext(this.state.title)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                                <div className="row">
                                    <div className="form-group col-xs-4">
                                        <label>Year*</label>
                                        <input
                                            type="number"
                                            required
                                            className="form-control"
                                            id="year"
                                            placeholder="Enter year"
                                            value={this.validateContext(this.state.year)}
                                            onChange={this.propertyChanged}
                                        />
                                    </div>
                                    <div className="form-group col-xs-4">
                                        <label>Pages*</label>
                                        <input
                                            type="number"
                                            required
                                            className="form-control"
                                            id="pages"
                                            placeholder="Enter number of pages"
                                            value={this.validateContext(this.state.pages)}
                                            onChange={this.propertyChanged}
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Publisher</label>
                                    <select
                                        className="form-control"
                                        id="publisher_id"
                                        onChange={this.propertyChanged}
                                        value={this.validateContext(this.state.publisher_id)}
                                    >
                                        <option value='' disabled> Choose... </option>
                                        {/* Here we have to got the publishers and list them by name and take the value of the id and store it to publisher_id */}
                                        {this.state.publishers.map((item, index) => 
                                            <option value={item.id} key={index}>{item.name}</option>
                                        )}
                                    </select>
                                </div>
                            </div>
                            <div className="box-footer">
                                <button
                                    type="submit"
                                    className="btn btn-primary"
                                    onClick={e =>
                                        this.registerBook.bind(this)(e, container)
                                    }
                                >
                                    Submit
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            );
    }
}

export default BookNew;
