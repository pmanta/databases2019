import React, { Component } from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import { update_state } from './FunctionLibrary'

class PageDecorator extends Component {
    state = {}

    constructor(props) {
        super(props)
        this.state = {
            scriptLoaded: false,
        }
        this.update_state = update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false
    }

    render() {
        const { render } = this.props
        return (
            <div className="wrapper">
                <Header/>
                <aside className="main-sidebar">
                    <Sidebar/>
                </aside>
                <div className="content-wrapper">
                    {render()}
                </div>
            </div>
        )
    }
}

export default PageDecorator;