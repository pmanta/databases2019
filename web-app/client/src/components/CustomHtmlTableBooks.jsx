import React, { Component } from 'react'
import '../css/custom.css'
import {Link} from 'react-router-dom'

class CustomHtmlTableBooks extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const { books } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>ISBN</th>
                                <th>Title</th>
                                <th>Year</th>
                                <th>Pages</th>
                                <th>Publisher ID</th>
                            </tr>

                            {books.map((item, index) => (
                                <tr key={index}>
                                    <td>
                                        <Link to={`/books/${item.isbn}`}>
                                            {item.isbn}
                                        </Link>
                                    </td>
                                    <td>{item.title}</td>
                                    <td>{item.year}</td>
                                    <td>{item.pages}</td>
                                    <td>
                                        <Link to={`/publishers/${item.publisher_id}`}>
                                            {item.publisher_id}
                                        </Link>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTableBooks
