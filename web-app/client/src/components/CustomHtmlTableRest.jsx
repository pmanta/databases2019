import React, { Component } from 'react'
import '../css/custom.css'

class CustomHtmlTableRest extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        // Get the keys from the 1st object and follow along to the other ones!!
        var keys = Object.keys(this.props.data[0]);

        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                {keys.map((item, index) => 
                                    <th key={index}>{item}</th>    
                                )}
                            </tr>

                            {this.props.data.map((item, index) => (
                                <tr key={index}>
                                    {keys.map((it, ind) => 
                                        <td key={`${index},${ind}`}>{item[it]}</td>
                                    )}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTableRest