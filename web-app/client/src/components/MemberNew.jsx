import React, { Component } from "react";
import * as functions from "./FunctionLibrary";
import { ToastContainer } from "react-toastr";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

class MemberNew extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    registerMember(e, toastContainer) {
        const make2digit = functions.make2digit;

        if (
            !!!this.state.name ||
            !!!this.state.surname ||
            !!!this.state.address ||
            !!!this.state.birthDay
        ) {
            toastContainer.error("Input form data are invalid", "Error", {
                closeButton: true
            });
            return;
        }

        var year = this.state.birthDay.getFullYear();
        var month = this.state.birthDay.getMonth() + 1;
        var day = this.state.birthDay.getDate();

        var date = year + '-' + make2digit(month) + '-' + make2digit(day);

        var obj = {
            name: this.state.name,
            surname: this.state.surname,
            address: this.state.address,
            birthDay: date,
        };

        console.log(obj);
        functions
            .http_post("/members", JSON.stringify(obj))
            .then(json => {
                console.log("got ", json);
                toastContainer.success("Registered member.", "Success!", {
                    closeButton: true
                });

                // Empty all fields in the form for adding a new shop
                this.update_state({
                    name: '',
                    surname: '',
                    address: '',
                    birthDay: undefined
                });
            })
            .catch(status => {
                console.log("what is this?");
                toastContainer.error("Status " + status, "Error!", {
                    closeButton: true
                });
            });
    }

    render() {
        let container;
        return (
            <div>
                <ToastContainer
                    ref={ref => (container = ref)}
                    className="toast-top-right"
                />

                <section className="content-header">
                    <h1>
                        Member
                        <small>add new member</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="surname">Surname*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="surname"
                                    placeholder="Enter surname"
                                    value={this.validateContext(this.state.surname)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="address">Address*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="address"
                                    placeholder="Enter address"
                                    value={this.validateContext(this.state.address)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="birthday">Birthday*</label>
                                <DatePicker
                                    className="form-control"
                                    selected={this.state.birthDay}
                                    onChange={(date) => this.update_state.bind(this)({ birthDay: date })}
                                    dateFormat="dd/MM/yyyy"
                                />
                            </div>
                        </div>
                        <div className="box-footer">
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={e =>
                                    this.registerMember.bind(this)(e, container)
                                }
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default MemberNew;
