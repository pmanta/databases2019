import React, { Component } from 'react'
import '../css/custom.css'
import {Link} from 'react-router-dom'

class CustomHtmlTableMembers extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const { members } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Address</th>
                                <th>Birthday</th>
                            </tr>

                            {members.map((item, index) => (
                                <tr key={index}>
                                    <td>
                                        <Link to={`/members/${item.id}`}>
                                            {item.id}
                                        </Link>
                                    </td>
                                    <td>{item.name}</td>
                                    <td>{item.surname}</td>
                                    <td>{item.address}</td>
                                    <td>{item.birthDay}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTableMembers