import React, { Component } from 'react';
import Loading from './Loading';
import * as functions from './FunctionLibrary';
import { ToastContainer } from 'react-toastr';
import { Redirect } from 'react-router-dom';

class BookDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            redirect: false,
            publishers: []
        }

        this.update_state = functions.update_state.bind(this);
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
    }
    
    componentDidMount() {
        this.mounted = true;
        var { handle } = this.props.match.params
        // Get the product details and show them in the register form
        functions.http_get_json("/books/" + handle)
            .then(json => {
                // add isloaded true to json :D
                json["isLoaded"] = true;
                this.update_state(json);
            })
        
        functions.http_get_json('/publishers')
            .then(json => {
                this.update_state({
                    publishers: json
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    
    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    updateBook(e, toastContainer) {
        console.log("update book kappa")
        var { handle } = this.props.match.params;

        if (
            !!isNaN(this.state.isbn) ||
            !!!this.state.title ||
            !!isNaN(this.state.year) ||
            !!isNaN(this.state.pages) ||
            !!isNaN(this.state.publisher_id)) 
        {
            toastContainer.error("Fill all required fields", "Error", {
                closeButton: true
            })
            return;
        }

        var obj = {
            isbn: parseInt(this.state.isbn),
            title: this.state.title,
            year: parseInt(this.state.year),
            pages: parseInt(this.state.pages),
            publisher_id: parseInt(this.state.publisher_id),
        }

        console.log(obj);
        functions.http_put("/books/" + handle, JSON.stringify(obj))
            .then(json => {
                toastContainer.success("updated book", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/books' />
        }
    }

    deleteBook(e, toastContainer) {
        var { handle } = this.props.match.params
        if (window.confirm("Are you sure you want to delete the book ?"))
            console.log("deleting book with id :", handle)

        functions.http_delete("/books/" + handle)
            .then(json => {
                toastContainer.success("deleted book", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    render() {
        let container;
        if (!this.state.isLoaded) return (<Loading/>)
        return (
            <div>
                {this.renderRedirect()}
                <ToastContainer
                    ref={ref => container = ref}
                    className="toast-top-right"
                />
                
                <section className="content-header">
                    <h1>
                        Book
                        <small>update book</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">ISBN*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="isbn"
                                    placeholder="Enter isbn"
                                    value={this.validateContext(this.state.isbn)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="address">Title*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="title"
                                    placeholder="Enter Title"
                                    value={this.validateContext(this.state.title)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-4">
                                    <label>Year*</label>
                                    <input
                                        type="number"
                                        required
                                        className="form-control"
                                        id="year"
                                        placeholder="Enter year"
                                        value={this.validateContext(this.state.year)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                                <div className="form-group col-xs-4">
                                    <label>Pages*</label>
                                    <input
                                        type="number"
                                        required
                                        className="form-control"
                                        id="pages"
                                        placeholder="Enter number of pages"
                                        value={this.validateContext(this.state.pages)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Publisher</label>
                                <select
                                    className="form-control"
                                    id="publisher_id"
                                    onChange={this.propertyChanged}
                                    value={this.validateContext(this.state.publisher_id)}
                                >
                                    <option value='' disabled> Choose... </option>
                                    {/* Here we have to got the publishers and list them by name and take the value of the id and store it to publisher_id */}
                                    {this.state.publishers.map((item, index) => 
                                        <option value={item.id} key={index}>{item.name}</option>
                                    )}
                                </select>
                            </div>
                        </div>
                        <div className="box-footer">
                            <button 
                                type="submit" 
                                className="btn btn-primary"
                                onClick={(e) => this.updateBook.bind(this)(e, container)}
                            >
                                Update
                            </button>
                            &nbsp;
                            <button
                                type="submit"
                                className="btn btn-danger"
                                onClick={() => this.update_state({ redirect:true })}
                            >
                                Cancel
                            </button>
                            <button
                                type="submit"
                                className="btn btn-warning pull-right"
                                onClick={(e) => this.deleteBook(e, container)}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                    
                </section>
            </div>
        );
    }
}
 
export default BookDetails;