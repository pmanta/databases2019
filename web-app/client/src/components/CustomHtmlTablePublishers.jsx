import React, { Component } from 'react'
import '../css/custom.css'
import {Link} from 'react-router-dom'

class CustomHtmlTablePublishers extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const { publishers } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Year</th>
                            </tr>

                            {publishers.map((item, index) => (
                                <tr key={index}>
                                    <td>
                                        <Link to={`/publishers/${item.id}`}>
                                            {item.id}
                                        </Link>
                                    </td>
                                    <td>{item.name}</td>
                                    <td>{item.address}</td>
                                    <td>{item.year}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTablePublishers