import React, { Component } from 'react';
import * as functions from './FunctionLibrary';
import Loading from './Loading';
import CustomHtmlTableRest from './CustomHtmlTableRest';

class GroupBy extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            loading: true,
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;

        functions.getQuery(4)
            .then((res) => {
                this.update_state({
                    result: res,
                    loading: false,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    render() {
        if (this.state.loading)
            return <Loading />

        return ( <div>
            <section className="content-header">
                <h1>Group By 
                    <small>preview of group by query</small>
                </h1>
            </section>
            <div className="callout callout-info pad margin no-print">
                <h4>
                    Get the number of copies for every book
                </h4>
                select book_isbn, count(*) as num_of_copies from cdatabases.copy
                group by book_isbn;
            </div>
            <CustomHtmlTableRest  data={this.state.result}/>
        </div> );
    }
}
 
export default GroupBy;