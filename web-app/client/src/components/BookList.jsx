import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTableBooks from './CustomHtmlTableBooks.jsx';

class BookList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            shops: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the shop List from the api
        functions.http_get_json("/books")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    books: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate() {
        console.log(this.state);
    }
    
    render() {
        const {isLoaded, books} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Books
                            <small>preview of books</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTableBooks books={books}/>
                    </section>
                </div>
            );
    }
}

export default BookList;