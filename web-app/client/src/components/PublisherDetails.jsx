import React, { Component } from 'react';
import Loading from './Loading';
import * as functions from './FunctionLibrary';
import { ToastContainer } from 'react-toastr';
import { Redirect } from 'react-router-dom';

class PublisherDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,        
        }

        this.update_state = functions.update_state.bind(this);
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
    }
    
    componentDidMount() {
        this.mounted = true;
        var { handle } = this.props.match.params
        // Get the product details and show them in the register form
        functions.http_get_json("/publishers/" + handle)
            .then(json => {
                // add isloaded true to json :D
                json["isLoaded"] = true;
                this.update_state(json);
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
    
    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    updatePublisher(e, toastContainer) {
        console.log("update publisher kappa")
        var { handle } = this.props.match.params;

        if (
            !!!this.state.name ||
            !!!this.state.address ||
            !!isNaN(this.state.year)
        ) {
            toastContainer.error("Fill all required fields", "Error", {
                closeButton: true
            })
            return;
        }

        var obj = {
            name: this.state.name,
            address: this.state.address,
            year: parseInt(this.state.year),
        };

        console.log(obj);
        functions.http_put("/publishers/" + handle, JSON.stringify(obj))
            .then(json => {
                toastContainer.success("updated publisher", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/publishers' />
        }
    }

    deletePublisher(e, toastContainer) {
        var { handle } = this.props.match.params
        if (window.confirm("Are you sure you want to delete the publisher ?"))
            console.log("deleting publisher with id :", handle)

        functions.http_delete("/publishers/" + handle)
            .then(json => {
                toastContainer.success("deleted publisher", "Success", {
                    closeButton: true
                })
            })
            .catch(status => {
                console.log("what is this?")
                toastContainer.error("Status " + status, "Error", {
                    closeButton: true
                })
            })
    }

    render() {
        let container;
        if (!this.state.isLoaded) return (<Loading/>)
        return (
            <div>
                {this.renderRedirect()}
                <ToastContainer
                    ref={ref => container = ref}
                    className="toast-top-right"
                />
                
                <section className="content-header">
                    <h1>
                        Publisher
                        <small>update publisher</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="address">Address*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="address"
                                    placeholder="Enter address"
                                    value={this.validateContext(this.state.address)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="year">Year*</label>
                                <input
                                    type="number"
                                    required
                                    className="form-control"
                                    id="year"
                                    placeholder="Enter year"
                                    value={this.validateContext(this.state.year)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                        </div>
                        <div className="box-footer">
                            <button 
                                type="submit" 
                                className="btn btn-primary"
                                onClick={(e) => this.updatePublisher.bind(this)(e, container)}
                            >
                                Update
                            </button>
                            &nbsp;
                            <button
                                type="submit"
                                className="btn btn-danger"
                                onClick={() => this.update_state({ redirect:true })}
                            >
                                Cancel
                            </button>
                            <button
                                type="submit"
                                className="btn btn-warning pull-right"
                                onClick={(e) => this.deletePublisher(e, container)}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                    
                </section>
            </div>
        );
    }
}
 
export default PublisherDetails;