import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTableAuthors from './CustomHtmlTableAuthors.jsx';

class AuthorList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            authors: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the product List from the api
        functions.http_get_json("/authors")
            .then(json => {
                this.update_state({
                    isLoaded: true,
                    authors: json,
                })
            })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const {isLoaded, authors} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Authors
                            <small>preview of authors</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTableAuthors authors={authors}/>
                    </section>
                </div>
            );
    }
}
 
export default AuthorList;