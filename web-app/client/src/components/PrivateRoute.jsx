import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import { update_state } from './FunctionLibrary.jsx'

class PrivateRoute extends Component {
    state = {}

    constructor(props) {
        super(props)
        this.state = {
            isAuthenticated: false,
        }
        this.updateState = update_state.bind(this)
    }

    componentDidMount() {
        this.mounted = true
    }

    componentWillUnmount() {
        this.mounted = false
    }

    render() {
        const {
            component: Component,
            render: RenderMethod,
            decorator: Decorator,
            ...rest
        } = this.props

        if (this.state.isLoading) return null
        else
            return (
                <Decorator
                    render={props => (
                        <Route
                            {...rest}
                            render={props => {
                                if (Component !== undefined)
                                    return <Component {...props} />
                                else return RenderMethod()
                            }}
                        />
                    )}
                />
            )
    }
}

export default PrivateRoute
