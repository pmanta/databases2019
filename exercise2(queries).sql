-- question 1 -- 
SELECT message FROM ex2schema.message order by length(message) DESC LIMIT 5;

-- question 2 --
select count(*) from (ex2schema.message inner join ex2schema.users on sender_uid=uid) where phone like '210%';

-- question 3 --
SELECT s.name from
((select uid, name, count(*) as sent from (ex2schema.message join ex2schema.users on sender_uid=uid) group by uid) as s
inner join 
(select uid, name, count(*) as received from (ex2schema.message join ex2schema.users on recipient_uid=uid) group by uid) as r on s.uid=r.uid) where sent > received;

-- question 4 --
select distinct s.message from
(select distinct recipient_uid,sender_uid,message from ex2schema.message where message in (select message from ex2schema.message group by message having count(message) >= 2)) as s;

-- question 5 --
select avg(average) from 
(select uid, name, count(*) as sent, avg(length(message)) as average from (ex2schema.message join ex2schema.users on sender_uid=uid) group by uid order by sent DESC LIMIT 5) as s;

-- question 6 --
select distinct name from (ex2schema.message inner join ex2schema.users on sender_uid=uid)
where recipient_uid in
(select distinct recipient_uid from
(ex2schema.message inner join ex2schema.users on sender_uid=uid and name='Tsipras')) and name != 'Tsipras'
