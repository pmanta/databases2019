CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `cdatabases`.`book_information_view` AS
    SELECT 
        `b`.`isbn` AS `isbn`,
        `b`.`title` AS `title`,
        `b`.`year` AS `year`,
        `b`.`pages` AS `pages`,
        `p`.`name` AS `publisher_name`,
        `p`.`address` AS `publisher_address`,
        GROUP_CONCAT(`a`.`name`
            SEPARATOR ',') AS `authors`
    FROM
        (((`cdatabases`.`book` `b`
        JOIN `cdatabases`.`publisher` `p` ON ((`p`.`id` = `b`.`publisher_id`)))
        JOIN `cdatabases`.`book_has_author` `bha` ON ((`bha`.`book_isbn` = `b`.`isbn`)))
        JOIN `cdatabases`.`author` `a` ON ((`bha`.`author_id` = `a`.`id`)))
    GROUP BY `b`.`isbn`